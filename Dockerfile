FROM registry.gitlab.com/itomychstudio/builders/terraform.builder:1
ADD https://github.com/gruntwork-io/terragrunt/releases/download/v0.35.16/terragrunt_linux_amd64 /usr/local/bin/terragrunt
ADD https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/terragrunt